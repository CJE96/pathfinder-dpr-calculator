﻿using System;
using System.Drawing;

namespace DPR_WinForm
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.dType0 = new System.Windows.Forms.ComboBox();
            this.dLabel = new System.Windows.Forms.Label();
            this.dNumberLabel = new System.Windows.Forms.Label();
            this.tohitLabel = new System.Windows.Forms.Label();
            this.dTypeLabel = new System.Windows.Forms.Label();
            this.dModifierLabel = new System.Windows.Forms.Label();
            this.dType1 = new System.Windows.Forms.ComboBox();
            this.dNumber0 = new System.Windows.Forms.NumericUpDown();
            this.dNumber1 = new System.Windows.Forms.NumericUpDown();
            this.tohitTwenty = new System.Windows.Forms.Label();
            this.attackModifierNumber = new System.Windows.Forms.NumericUpDown();
            this.dModifier0 = new System.Windows.Forms.NumericUpDown();
            this.dThreatLabel = new System.Windows.Forms.Label();
            this.dThreat0 = new System.Windows.Forms.ComboBox();
            this.dCritLabel = new System.Windows.Forms.Label();
            this.dCrit0 = new System.Windows.Forms.ComboBox();
            this.calculateButton = new System.Windows.Forms.Button();
            this.resultsLabel = new System.Windows.Forms.Label();
            this.acTextBox = new System.Windows.Forms.TextBox();
            this.hitPercentTextBox = new System.Windows.Forms.TextBox();
            this.acLabel = new System.Windows.Forms.Label();
            this.hitPercentLabel = new System.Windows.Forms.Label();
            this.dprLabel = new System.Windows.Forms.Label();
            this.dprTextBox = new System.Windows.Forms.TextBox();
            this.acStartLabel = new System.Windows.Forms.Label();
            this.acStartNumber = new System.Windows.Forms.NumericUpDown();
            this.dType2 = new System.Windows.Forms.ComboBox();
            this.dNumber2 = new System.Windows.Forms.NumericUpDown();
            this.dNumber3 = new System.Windows.Forms.NumericUpDown();
            this.dType3 = new System.Windows.Forms.ComboBox();
            this.dType4 = new System.Windows.Forms.ComboBox();
            this.dNumber4 = new System.Windows.Forms.NumericUpDown();
            this.dHitBonus = new System.Windows.Forms.Label();
            this.dCritBonus = new System.Windows.Forms.Label();
            this.dCritNumber3 = new System.Windows.Forms.NumericUpDown();
            this.dCritType3 = new System.Windows.Forms.ComboBox();
            this.dCritNumber0 = new System.Windows.Forms.NumericUpDown();
            this.dCritNumber1 = new System.Windows.Forms.NumericUpDown();
            this.dCritNumber2 = new System.Windows.Forms.NumericUpDown();
            this.dCritType2 = new System.Windows.Forms.ComboBox();
            this.dCritType1 = new System.Windows.Forms.ComboBox();
            this.dCritType0 = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dNumber0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dNumber1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.attackModifierNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dModifier0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.acStartNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dNumber2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dNumber3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dNumber4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCritNumber3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCritNumber0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCritNumber1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCritNumber2)).BeginInit();
            this.SuspendLayout();
            // 
            // dType0
            // 
            this.dType0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(46)))), ((int)(((byte)(53)))));
            this.dType0.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dType0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dType0.Font = new System.Drawing.Font("Source Sans Pro Semibold", 10F);
            this.dType0.ForeColor = System.Drawing.Color.White;
            this.dType0.FormattingEnabled = true;
            this.dType0.Items.AddRange(new object[] {
            "d4",
            "d6",
            "d8",
            "d10",
            "d12"});
            this.dType0.Location = new System.Drawing.Point(101, 119);
            this.dType0.Name = "dType0";
            this.dType0.Size = new System.Drawing.Size(46, 25);
            this.dType0.TabIndex = 3;
            // 
            // dLabel
            // 
            this.dLabel.AutoSize = true;
            this.dLabel.Font = new System.Drawing.Font("Source Sans Pro Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dLabel.ForeColor = System.Drawing.Color.White;
            this.dLabel.Location = new System.Drawing.Point(20, 72);
            this.dLabel.Name = "dLabel";
            this.dLabel.Size = new System.Drawing.Size(140, 20);
            this.dLabel.TabIndex = 0;
            this.dLabel.Text = "Weapon Properties";
            // 
            // dNumberLabel
            // 
            this.dNumberLabel.AutoSize = true;
            this.dNumberLabel.Font = new System.Drawing.Font("Source Sans Pro", 10F);
            this.dNumberLabel.ForeColor = System.Drawing.Color.White;
            this.dNumberLabel.Location = new System.Drawing.Point(21, 97);
            this.dNumberLabel.Name = "dNumberLabel";
            this.dNumberLabel.Size = new System.Drawing.Size(57, 18);
            this.dNumberLabel.TabIndex = 4;
            this.dNumberLabel.Text = "Number";
            // 
            // tohitLabel
            // 
            this.tohitLabel.AutoSize = true;
            this.tohitLabel.Font = new System.Drawing.Font("Source Sans Pro Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tohitLabel.ForeColor = System.Drawing.Color.White;
            this.tohitLabel.Location = new System.Drawing.Point(20, 12);
            this.tohitLabel.Name = "tohitLabel";
            this.tohitLabel.Size = new System.Drawing.Size(82, 20);
            this.tohitLabel.TabIndex = 5;
            this.tohitLabel.Text = "Attack Roll";
            // 
            // dTypeLabel
            // 
            this.dTypeLabel.AutoSize = true;
            this.dTypeLabel.Font = new System.Drawing.Font("Source Sans Pro", 10F);
            this.dTypeLabel.ForeColor = System.Drawing.Color.White;
            this.dTypeLabel.Location = new System.Drawing.Point(98, 97);
            this.dTypeLabel.Name = "dTypeLabel";
            this.dTypeLabel.Size = new System.Drawing.Size(38, 18);
            this.dTypeLabel.TabIndex = 6;
            this.dTypeLabel.Text = "Type";
            // 
            // dModifierLabel
            // 
            this.dModifierLabel.AutoSize = true;
            this.dModifierLabel.Font = new System.Drawing.Font("Source Sans Pro", 10F);
            this.dModifierLabel.ForeColor = System.Drawing.Color.White;
            this.dModifierLabel.Location = new System.Drawing.Point(167, 97);
            this.dModifierLabel.Name = "dModifierLabel";
            this.dModifierLabel.Size = new System.Drawing.Size(56, 18);
            this.dModifierLabel.TabIndex = 7;
            this.dModifierLabel.Text = "Modifier";
            // 
            // dType1
            // 
            this.dType1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(46)))), ((int)(((byte)(53)))));
            this.dType1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dType1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dType1.Font = new System.Drawing.Font("Source Sans Pro Semibold", 10F);
            this.dType1.ForeColor = System.Drawing.Color.White;
            this.dType1.FormattingEnabled = true;
            this.dType1.Items.AddRange(new object[] {
            "d4",
            "d6",
            "d8",
            "d10",
            "d12"});
            this.dType1.Location = new System.Drawing.Point(101, 179);
            this.dType1.Name = "dType1";
            this.dType1.Size = new System.Drawing.Size(46, 25);
            this.dType1.TabIndex = 8;
            // 
            // dNumber0
            // 
            this.dNumber0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(46)))), ((int)(((byte)(53)))));
            this.dNumber0.Font = new System.Drawing.Font("Source Sans Pro Semibold", 10F);
            this.dNumber0.ForeColor = System.Drawing.Color.White;
            this.dNumber0.Location = new System.Drawing.Point(24, 120);
            this.dNumber0.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.dNumber0.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.dNumber0.Name = "dNumber0";
            this.dNumber0.Size = new System.Drawing.Size(36, 24);
            this.dNumber0.TabIndex = 2;
            this.dNumber0.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // dNumber1
            // 
            this.dNumber1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(46)))), ((int)(((byte)(53)))));
            this.dNumber1.Font = new System.Drawing.Font("Source Sans Pro Semibold", 10F);
            this.dNumber1.ForeColor = System.Drawing.Color.White;
            this.dNumber1.Location = new System.Drawing.Point(24, 180);
            this.dNumber1.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.dNumber1.Name = "dNumber1";
            this.dNumber1.Size = new System.Drawing.Size(36, 24);
            this.dNumber1.TabIndex = 7;
            // 
            // tohitTwenty
            // 
            this.tohitTwenty.AutoSize = true;
            this.tohitTwenty.Font = new System.Drawing.Font("Source Sans Pro", 10F);
            this.tohitTwenty.ForeColor = System.Drawing.Color.White;
            this.tohitTwenty.Location = new System.Drawing.Point(21, 42);
            this.tohitTwenty.Name = "tohitTwenty";
            this.tohitTwenty.Size = new System.Drawing.Size(43, 18);
            this.tohitTwenty.TabIndex = 15;
            this.tohitTwenty.Text = "d20  +";
            // 
            // attackModifierNumber
            // 
            this.attackModifierNumber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(46)))), ((int)(((byte)(53)))));
            this.attackModifierNumber.Font = new System.Drawing.Font("Source Sans Pro Semibold", 10F);
            this.attackModifierNumber.ForeColor = System.Drawing.Color.White;
            this.attackModifierNumber.Location = new System.Drawing.Point(66, 40);
            this.attackModifierNumber.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.attackModifierNumber.Name = "attackModifierNumber";
            this.attackModifierNumber.Size = new System.Drawing.Size(50, 24);
            this.attackModifierNumber.TabIndex = 0;
            // 
            // dModifier0
            // 
            this.dModifier0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(46)))), ((int)(((byte)(53)))));
            this.dModifier0.Font = new System.Drawing.Font("Source Sans Pro Semibold", 10F);
            this.dModifier0.ForeColor = System.Drawing.Color.White;
            this.dModifier0.Location = new System.Drawing.Point(170, 121);
            this.dModifier0.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.dModifier0.Name = "dModifier0";
            this.dModifier0.Size = new System.Drawing.Size(50, 24);
            this.dModifier0.TabIndex = 4;
            // 
            // dThreatLabel
            // 
            this.dThreatLabel.AutoSize = true;
            this.dThreatLabel.Font = new System.Drawing.Font("Source Sans Pro", 10F);
            this.dThreatLabel.ForeColor = System.Drawing.Color.White;
            this.dThreatLabel.Location = new System.Drawing.Point(237, 97);
            this.dThreatLabel.Name = "dThreatLabel";
            this.dThreatLabel.Size = new System.Drawing.Size(88, 18);
            this.dThreatLabel.TabIndex = 21;
            this.dThreatLabel.Text = "Threat Range";
            // 
            // dThreat0
            // 
            this.dThreat0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(46)))), ((int)(((byte)(53)))));
            this.dThreat0.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dThreat0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dThreat0.Font = new System.Drawing.Font("Source Sans Pro Semibold", 10F);
            this.dThreat0.ForeColor = System.Drawing.Color.White;
            this.dThreat0.FormattingEnabled = true;
            this.dThreat0.Items.AddRange(new object[] {
            "20",
            "19-20",
            "18-20",
            "17-20",
            "15-20"});
            this.dThreat0.Location = new System.Drawing.Point(240, 119);
            this.dThreat0.Name = "dThreat0";
            this.dThreat0.Size = new System.Drawing.Size(68, 25);
            this.dThreat0.TabIndex = 5;
            // 
            // dCritLabel
            // 
            this.dCritLabel.AutoSize = true;
            this.dCritLabel.Font = new System.Drawing.Font("Source Sans Pro", 10F);
            this.dCritLabel.ForeColor = System.Drawing.Color.White;
            this.dCritLabel.Location = new System.Drawing.Point(345, 97);
            this.dCritLabel.Name = "dCritLabel";
            this.dCritLabel.Size = new System.Drawing.Size(89, 18);
            this.dCritLabel.TabIndex = 26;
            this.dCritLabel.Text = "Crit Multiplier";
            // 
            // dCrit0
            // 
            this.dCrit0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(46)))), ((int)(((byte)(53)))));
            this.dCrit0.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dCrit0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dCrit0.Font = new System.Drawing.Font("Source Sans Pro Semibold", 10F);
            this.dCrit0.ForeColor = System.Drawing.Color.White;
            this.dCrit0.FormattingEnabled = true;
            this.dCrit0.Items.AddRange(new object[] {
            "x2",
            "x3",
            "x4",
            "x6",
            "x8"});
            this.dCrit0.Location = new System.Drawing.Point(348, 119);
            this.dCrit0.Name = "dCrit0";
            this.dCrit0.Size = new System.Drawing.Size(42, 25);
            this.dCrit0.TabIndex = 6;
            // 
            // calculateButton
            // 
            this.calculateButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(46)))), ((int)(((byte)(53)))));
            this.calculateButton.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.calculateButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.calculateButton.Font = new System.Drawing.Font("Source Sans Pro Semibold", 10F);
            this.calculateButton.ForeColor = System.Drawing.Color.White;
            this.calculateButton.Location = new System.Drawing.Point(24, 318);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(112, 31);
            this.calculateButton.TabIndex = 23;
            this.calculateButton.Text = "Calculate DPR";
            this.calculateButton.UseVisualStyleBackColor = false;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // resultsLabel
            // 
            this.resultsLabel.AutoSize = true;
            this.resultsLabel.Font = new System.Drawing.Font("Source Sans Pro Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resultsLabel.ForeColor = System.Drawing.Color.White;
            this.resultsLabel.Location = new System.Drawing.Point(451, 12);
            this.resultsLabel.Name = "resultsLabel";
            this.resultsLabel.Size = new System.Drawing.Size(63, 20);
            this.resultsLabel.TabIndex = 42;
            this.resultsLabel.Text = "Results:";
            // 
            // acTextBox
            // 
            this.acTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(57)))), ((int)(((byte)(68)))));
            this.acTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.acTextBox.Cursor = System.Windows.Forms.Cursors.Default;
            this.acTextBox.Font = new System.Drawing.Font("Source Sans Pro Semibold", 10F);
            this.acTextBox.ForeColor = System.Drawing.Color.White;
            this.acTextBox.Location = new System.Drawing.Point(455, 63);
            this.acTextBox.Multiline = true;
            this.acTextBox.Name = "acTextBox";
            this.acTextBox.ReadOnly = true;
            this.acTextBox.Size = new System.Drawing.Size(49, 257);
            this.acTextBox.TabIndex = 24;
            this.acTextBox.Text = "Enemy:\r\n2:\r\n3:\r\n4:\r\n5:\r\n6:\r\n7:\r\n8:\r\n9:\r\n10:\r\n11:\r\n12:\r\n13:\r\n14:\r\n15:";
            // 
            // hitPercentTextBox
            // 
            this.hitPercentTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(46)))), ((int)(((byte)(53)))));
            this.hitPercentTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.hitPercentTextBox.Cursor = System.Windows.Forms.Cursors.Default;
            this.hitPercentTextBox.Font = new System.Drawing.Font("Source Sans Pro Semibold", 10F);
            this.hitPercentTextBox.ForeColor = System.Drawing.Color.White;
            this.hitPercentTextBox.Location = new System.Drawing.Point(510, 63);
            this.hitPercentTextBox.Multiline = true;
            this.hitPercentTextBox.Name = "hitPercentTextBox";
            this.hitPercentTextBox.ReadOnly = true;
            this.hitPercentTextBox.Size = new System.Drawing.Size(72, 257);
            this.hitPercentTextBox.TabIndex = 25;
            // 
            // acLabel
            // 
            this.acLabel.AutoSize = true;
            this.acLabel.Font = new System.Drawing.Font("Source Sans Pro", 10F);
            this.acLabel.ForeColor = System.Drawing.Color.White;
            this.acLabel.Location = new System.Drawing.Point(452, 42);
            this.acLabel.Name = "acLabel";
            this.acLabel.Size = new System.Drawing.Size(52, 18);
            this.acLabel.TabIndex = 45;
            this.acLabel.Text = "AC/TAC";
            // 
            // hitPercentLabel
            // 
            this.hitPercentLabel.AutoSize = true;
            this.hitPercentLabel.Font = new System.Drawing.Font("Source Sans Pro", 10F);
            this.hitPercentLabel.ForeColor = System.Drawing.Color.White;
            this.hitPercentLabel.Location = new System.Drawing.Point(507, 42);
            this.hitPercentLabel.Name = "hitPercentLabel";
            this.hitPercentLabel.Size = new System.Drawing.Size(72, 18);
            this.hitPercentLabel.TabIndex = 46;
            this.hitPercentLabel.Text = "Hit Chance";
            // 
            // dprLabel
            // 
            this.dprLabel.AutoSize = true;
            this.dprLabel.Font = new System.Drawing.Font("Source Sans Pro", 10F);
            this.dprLabel.ForeColor = System.Drawing.Color.White;
            this.dprLabel.Location = new System.Drawing.Point(585, 42);
            this.dprLabel.Name = "dprLabel";
            this.dprLabel.Size = new System.Drawing.Size(33, 18);
            this.dprLabel.TabIndex = 47;
            this.dprLabel.Text = "DPR";
            // 
            // dprTextBox
            // 
            this.dprTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(46)))), ((int)(((byte)(53)))));
            this.dprTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dprTextBox.Cursor = System.Windows.Forms.Cursors.Default;
            this.dprTextBox.Font = new System.Drawing.Font("Source Sans Pro Semibold", 10F);
            this.dprTextBox.ForeColor = System.Drawing.Color.White;
            this.dprTextBox.Location = new System.Drawing.Point(588, 63);
            this.dprTextBox.Multiline = true;
            this.dprTextBox.Name = "dprTextBox";
            this.dprTextBox.ReadOnly = true;
            this.dprTextBox.Size = new System.Drawing.Size(72, 257);
            this.dprTextBox.TabIndex = 26;
            // 
            // acStartLabel
            // 
            this.acStartLabel.AutoSize = true;
            this.acStartLabel.Font = new System.Drawing.Font("Source Sans Pro Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.acStartLabel.ForeColor = System.Drawing.Color.White;
            this.acStartLabel.Location = new System.Drawing.Point(236, 12);
            this.acStartLabel.Name = "acStartLabel";
            this.acStartLabel.Size = new System.Drawing.Size(109, 20);
            this.acStartLabel.TabIndex = 49;
            this.acStartLabel.Text = "Enemy AC/TAC";
            // 
            // acStartNumber
            // 
            this.acStartNumber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(46)))), ((int)(((byte)(53)))));
            this.acStartNumber.Font = new System.Drawing.Font("Source Sans Pro Semibold", 10F);
            this.acStartNumber.ForeColor = System.Drawing.Color.White;
            this.acStartNumber.Location = new System.Drawing.Point(240, 40);
            this.acStartNumber.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.acStartNumber.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.acStartNumber.Name = "acStartNumber";
            this.acStartNumber.Size = new System.Drawing.Size(68, 24);
            this.acStartNumber.TabIndex = 1;
            this.acStartNumber.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // dType2
            // 
            this.dType2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(46)))), ((int)(((byte)(53)))));
            this.dType2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dType2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dType2.Font = new System.Drawing.Font("Source Sans Pro Semibold", 10F);
            this.dType2.ForeColor = System.Drawing.Color.White;
            this.dType2.FormattingEnabled = true;
            this.dType2.Items.AddRange(new object[] {
            "d4",
            "d6",
            "d8",
            "d10",
            "d12"});
            this.dType2.Location = new System.Drawing.Point(101, 210);
            this.dType2.Name = "dType2";
            this.dType2.Size = new System.Drawing.Size(46, 25);
            this.dType2.TabIndex = 10;
            // 
            // dNumber2
            // 
            this.dNumber2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(46)))), ((int)(((byte)(53)))));
            this.dNumber2.Font = new System.Drawing.Font("Source Sans Pro Semibold", 10F);
            this.dNumber2.ForeColor = System.Drawing.Color.White;
            this.dNumber2.Location = new System.Drawing.Point(24, 211);
            this.dNumber2.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.dNumber2.Name = "dNumber2";
            this.dNumber2.Size = new System.Drawing.Size(36, 24);
            this.dNumber2.TabIndex = 9;
            // 
            // dNumber3
            // 
            this.dNumber3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(46)))), ((int)(((byte)(53)))));
            this.dNumber3.Font = new System.Drawing.Font("Source Sans Pro Semibold", 10F);
            this.dNumber3.ForeColor = System.Drawing.Color.White;
            this.dNumber3.Location = new System.Drawing.Point(24, 242);
            this.dNumber3.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.dNumber3.Name = "dNumber3";
            this.dNumber3.Size = new System.Drawing.Size(36, 24);
            this.dNumber3.TabIndex = 11;
            // 
            // dType3
            // 
            this.dType3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(46)))), ((int)(((byte)(53)))));
            this.dType3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dType3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dType3.Font = new System.Drawing.Font("Source Sans Pro Semibold", 10F);
            this.dType3.ForeColor = System.Drawing.Color.White;
            this.dType3.FormattingEnabled = true;
            this.dType3.Items.AddRange(new object[] {
            "d4",
            "d6",
            "d8",
            "d10",
            "d12"});
            this.dType3.Location = new System.Drawing.Point(101, 241);
            this.dType3.Name = "dType3";
            this.dType3.Size = new System.Drawing.Size(46, 25);
            this.dType3.TabIndex = 12;
            // 
            // dType4
            // 
            this.dType4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(46)))), ((int)(((byte)(53)))));
            this.dType4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dType4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dType4.Font = new System.Drawing.Font("Source Sans Pro Semibold", 10F);
            this.dType4.ForeColor = System.Drawing.Color.White;
            this.dType4.FormattingEnabled = true;
            this.dType4.Items.AddRange(new object[] {
            "d4",
            "d6",
            "d8",
            "d10",
            "d12"});
            this.dType4.Location = new System.Drawing.Point(101, 272);
            this.dType4.Name = "dType4";
            this.dType4.Size = new System.Drawing.Size(46, 25);
            this.dType4.TabIndex = 14;
            // 
            // dNumber4
            // 
            this.dNumber4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(46)))), ((int)(((byte)(53)))));
            this.dNumber4.Font = new System.Drawing.Font("Source Sans Pro Semibold", 10F);
            this.dNumber4.ForeColor = System.Drawing.Color.White;
            this.dNumber4.Location = new System.Drawing.Point(24, 273);
            this.dNumber4.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.dNumber4.Name = "dNumber4";
            this.dNumber4.Size = new System.Drawing.Size(36, 24);
            this.dNumber4.TabIndex = 13;
            // 
            // dHitBonus
            // 
            this.dHitBonus.AutoSize = true;
            this.dHitBonus.Font = new System.Drawing.Font("Source Sans Pro", 10F);
            this.dHitBonus.ForeColor = System.Drawing.Color.White;
            this.dHitBonus.Location = new System.Drawing.Point(21, 158);
            this.dHitBonus.Name = "dHitBonus";
            this.dHitBonus.Size = new System.Drawing.Size(146, 18);
            this.dHitBonus.TabIndex = 51;
            this.dHitBonus.Text = "Damage Bonus (On Hit)";
            // 
            // dCritBonus
            // 
            this.dCritBonus.AutoSize = true;
            this.dCritBonus.Font = new System.Drawing.Font("Source Sans Pro", 10F);
            this.dCritBonus.ForeColor = System.Drawing.Color.White;
            this.dCritBonus.Location = new System.Drawing.Point(237, 158);
            this.dCritBonus.Name = "dCritBonus";
            this.dCritBonus.Size = new System.Drawing.Size(150, 18);
            this.dCritBonus.TabIndex = 60;
            this.dCritBonus.Text = "Damage Bonus (On Crit)";
            // 
            // dCritNumber3
            // 
            this.dCritNumber3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(46)))), ((int)(((byte)(53)))));
            this.dCritNumber3.Font = new System.Drawing.Font("Source Sans Pro Semibold", 10F);
            this.dCritNumber3.ForeColor = System.Drawing.Color.White;
            this.dCritNumber3.Location = new System.Drawing.Point(240, 273);
            this.dCritNumber3.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.dCritNumber3.Name = "dCritNumber3";
            this.dCritNumber3.Size = new System.Drawing.Size(36, 24);
            this.dCritNumber3.TabIndex = 21;
            // 
            // dCritType3
            // 
            this.dCritType3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(46)))), ((int)(((byte)(53)))));
            this.dCritType3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dCritType3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dCritType3.Font = new System.Drawing.Font("Source Sans Pro Semibold", 10F);
            this.dCritType3.ForeColor = System.Drawing.Color.White;
            this.dCritType3.FormattingEnabled = true;
            this.dCritType3.Items.AddRange(new object[] {
            "d4",
            "d6",
            "d8",
            "d10",
            "d12"});
            this.dCritType3.Location = new System.Drawing.Point(317, 272);
            this.dCritType3.Name = "dCritType3";
            this.dCritType3.Size = new System.Drawing.Size(46, 25);
            this.dCritType3.TabIndex = 22;
            // 
            // dCritNumber0
            // 
            this.dCritNumber0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(46)))), ((int)(((byte)(53)))));
            this.dCritNumber0.Font = new System.Drawing.Font("Source Sans Pro Semibold", 10F);
            this.dCritNumber0.ForeColor = System.Drawing.Color.White;
            this.dCritNumber0.Location = new System.Drawing.Point(240, 180);
            this.dCritNumber0.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.dCritNumber0.Name = "dCritNumber0";
            this.dCritNumber0.Size = new System.Drawing.Size(36, 24);
            this.dCritNumber0.TabIndex = 15;
            // 
            // dCritNumber1
            // 
            this.dCritNumber1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(46)))), ((int)(((byte)(53)))));
            this.dCritNumber1.Font = new System.Drawing.Font("Source Sans Pro Semibold", 10F);
            this.dCritNumber1.ForeColor = System.Drawing.Color.White;
            this.dCritNumber1.Location = new System.Drawing.Point(240, 211);
            this.dCritNumber1.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.dCritNumber1.Name = "dCritNumber1";
            this.dCritNumber1.Size = new System.Drawing.Size(36, 24);
            this.dCritNumber1.TabIndex = 17;
            // 
            // dCritNumber2
            // 
            this.dCritNumber2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(46)))), ((int)(((byte)(53)))));
            this.dCritNumber2.Font = new System.Drawing.Font("Source Sans Pro Semibold", 10F);
            this.dCritNumber2.ForeColor = System.Drawing.Color.White;
            this.dCritNumber2.Location = new System.Drawing.Point(240, 242);
            this.dCritNumber2.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.dCritNumber2.Name = "dCritNumber2";
            this.dCritNumber2.Size = new System.Drawing.Size(36, 24);
            this.dCritNumber2.TabIndex = 19;
            // 
            // dCritType2
            // 
            this.dCritType2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(46)))), ((int)(((byte)(53)))));
            this.dCritType2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dCritType2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dCritType2.Font = new System.Drawing.Font("Source Sans Pro Semibold", 10F);
            this.dCritType2.ForeColor = System.Drawing.Color.White;
            this.dCritType2.FormattingEnabled = true;
            this.dCritType2.Items.AddRange(new object[] {
            "d4",
            "d6",
            "d8",
            "d10",
            "d12"});
            this.dCritType2.Location = new System.Drawing.Point(317, 241);
            this.dCritType2.Name = "dCritType2";
            this.dCritType2.Size = new System.Drawing.Size(46, 25);
            this.dCritType2.TabIndex = 20;
            // 
            // dCritType1
            // 
            this.dCritType1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(46)))), ((int)(((byte)(53)))));
            this.dCritType1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dCritType1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dCritType1.Font = new System.Drawing.Font("Source Sans Pro Semibold", 10F);
            this.dCritType1.ForeColor = System.Drawing.Color.White;
            this.dCritType1.FormattingEnabled = true;
            this.dCritType1.Items.AddRange(new object[] {
            "d4",
            "d6",
            "d8",
            "d10",
            "d12"});
            this.dCritType1.Location = new System.Drawing.Point(317, 210);
            this.dCritType1.Name = "dCritType1";
            this.dCritType1.Size = new System.Drawing.Size(46, 25);
            this.dCritType1.TabIndex = 18;
            // 
            // dCritType0
            // 
            this.dCritType0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(46)))), ((int)(((byte)(53)))));
            this.dCritType0.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dCritType0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dCritType0.Font = new System.Drawing.Font("Source Sans Pro Semibold", 10F);
            this.dCritType0.ForeColor = System.Drawing.Color.White;
            this.dCritType0.FormattingEnabled = true;
            this.dCritType0.Items.AddRange(new object[] {
            "d4",
            "d6",
            "d8",
            "d10",
            "d12"});
            this.dCritType0.Location = new System.Drawing.Point(317, 179);
            this.dCritType0.Name = "dCritType0";
            this.dCritType0.Size = new System.Drawing.Size(46, 25);
            this.dCritType0.TabIndex = 16;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(57)))), ((int)(((byte)(68)))));
            this.ClientSize = new System.Drawing.Size(684, 361);
            this.Controls.Add(this.dCritBonus);
            this.Controls.Add(this.dCritNumber3);
            this.Controls.Add(this.dCritType3);
            this.Controls.Add(this.dCritNumber0);
            this.Controls.Add(this.dCritNumber1);
            this.Controls.Add(this.dCritNumber2);
            this.Controls.Add(this.dCritType2);
            this.Controls.Add(this.dCritType1);
            this.Controls.Add(this.dCritType0);
            this.Controls.Add(this.dHitBonus);
            this.Controls.Add(this.acStartNumber);
            this.Controls.Add(this.acStartLabel);
            this.Controls.Add(this.dprTextBox);
            this.Controls.Add(this.dprLabel);
            this.Controls.Add(this.hitPercentLabel);
            this.Controls.Add(this.acLabel);
            this.Controls.Add(this.hitPercentTextBox);
            this.Controls.Add(this.acTextBox);
            this.Controls.Add(this.resultsLabel);
            this.Controls.Add(this.dNumber4);
            this.Controls.Add(this.dType4);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.dCrit0);
            this.Controls.Add(this.dCritLabel);
            this.Controls.Add(this.dThreat0);
            this.Controls.Add(this.dThreatLabel);
            this.Controls.Add(this.dModifier0);
            this.Controls.Add(this.attackModifierNumber);
            this.Controls.Add(this.tohitTwenty);
            this.Controls.Add(this.dNumber1);
            this.Controls.Add(this.dNumber2);
            this.Controls.Add(this.dNumber3);
            this.Controls.Add(this.dNumber0);
            this.Controls.Add(this.dType3);
            this.Controls.Add(this.dType2);
            this.Controls.Add(this.dType1);
            this.Controls.Add(this.dModifierLabel);
            this.Controls.Add(this.dTypeLabel);
            this.Controls.Add(this.tohitLabel);
            this.Controls.Add(this.dNumberLabel);
            this.Controls.Add(this.dLabel);
            this.Controls.Add(this.dType0);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(700, 400);
            this.MinimumSize = new System.Drawing.Size(600, 400);
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Pathfinder DPR Calculator";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dNumber0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dNumber1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.attackModifierNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dModifier0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.acStartNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dNumber2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dNumber3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dNumber4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCritNumber3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCritNumber0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCritNumber1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCritNumber2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        public System.Windows.Forms.ComboBox dType0;
        public System.Windows.Forms.Label dLabel;
        public System.Windows.Forms.Label dNumberLabel;
        public System.Windows.Forms.Label tohitLabel;
        public System.Windows.Forms.Label dTypeLabel;
        public System.Windows.Forms.Label dModifierLabel;
        public System.Windows.Forms.ComboBox dType1;
        private System.Windows.Forms.NumericUpDown dNumber0;
        private System.Windows.Forms.NumericUpDown dNumber1;
        public System.Windows.Forms.Label tohitTwenty;
        private System.Windows.Forms.NumericUpDown attackModifierNumber;
        private System.Windows.Forms.NumericUpDown dModifier0;
        public System.Windows.Forms.Label dThreatLabel;
        public System.Windows.Forms.ComboBox dThreat0;
        public System.Windows.Forms.Label dCritLabel;
        public System.Windows.Forms.ComboBox dCrit0;
        private System.Windows.Forms.Button calculateButton;
        public System.Windows.Forms.Label resultsLabel;
        private System.Windows.Forms.TextBox acTextBox;
        private System.Windows.Forms.TextBox hitPercentTextBox;
        public System.Windows.Forms.Label acLabel;
        public System.Windows.Forms.Label hitPercentLabel;
        public System.Windows.Forms.Label dprLabel;
        private System.Windows.Forms.TextBox dprTextBox;
        public System.Windows.Forms.Label acStartLabel;
        private System.Windows.Forms.NumericUpDown acStartNumber;
        public System.Windows.Forms.ComboBox dType2;
        private System.Windows.Forms.NumericUpDown dNumber2;
        private System.Windows.Forms.NumericUpDown dNumber3;
        public System.Windows.Forms.ComboBox dType3;
        public System.Windows.Forms.ComboBox dType4;
        private System.Windows.Forms.NumericUpDown dNumber4;
        public System.Windows.Forms.Label dHitBonus;
        public System.Windows.Forms.Label dCritBonus;
        private System.Windows.Forms.NumericUpDown dCritNumber3;
        public System.Windows.Forms.ComboBox dCritType3;
        private System.Windows.Forms.NumericUpDown dCritNumber0;
        private System.Windows.Forms.NumericUpDown dCritNumber1;
        private System.Windows.Forms.NumericUpDown dCritNumber2;
        public System.Windows.Forms.ComboBox dCritType2;
        public System.Windows.Forms.ComboBox dCritType1;
        public System.Windows.Forms.ComboBox dCritType0;
    }
}

