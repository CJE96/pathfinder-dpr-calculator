﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DPR_WinForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dType0.Text = "d4";
            dType1.Text = "d4";
            dType2.Text = "d4";
            dType3.Text = "d4";
            dType4.Text = "d4";

            dCritType0.Text = "d4";
            dCritType1.Text = "d4";
            dCritType2.Text = "d4";
            dCritType3.Text = "d4";

            dThreat0.Text = "20";
            
            dCrit0.Text = "x2";
        }

        //Methods for converting input strings into doubles with correct value
        #region
        public double getDiceType(string diceString)
        {
            double diceDouble;

            switch (diceString)
            {
                case "d4":
                    diceDouble = 4;
                    break;

                case "d6":
                    diceDouble = 6;
                    break;

                case "d8":
                    diceDouble = 8;
                    break;

                case "d10":
                    diceDouble = 10;
                    break;

                case "d12":
                    diceDouble = 12;
                    break;

                default:
                    diceDouble = 4;
                    break;
            }

            return diceDouble;
        }

        public double getCritMultiplier(string critString)
        {
            double critDouble;

            switch (critString)
            {

                case "x2":
                    critDouble = 2;
                    break;

                case "x3":
                    critDouble = 3;
                    break;

                case "x4":
                    critDouble = 4;
                    break;

                case "x6":
                    critDouble = 6;
                    break;

                case "x8":
                    critDouble = 8;
                    break;

                default:
                    critDouble = 2;
                    break;
            }

            return critDouble;
        }

        public double getThreatRange(string threatString)
        {
            double threatDouble;

            switch (threatString)
            {
                case "20":
                    threatDouble = 0.05; /// 1/20 = 0.5
                    break;

                case "19-20":
                    threatDouble = 0.1; /// 2/20 = 0.1
                    break;

                case "18-20":
                    threatDouble = 0.15; /// 3/20 = 0.15
                    break;

                case "17-20":
                    threatDouble = 0.2; /// 4/20 = 0.2
                    break;

                case "15-20":
                    threatDouble = 0.3; /// 6/20 = 0.3
                    break;

                default:
                    threatDouble = 0.05; /// defaults to threat range of 20
                    break;
            }

            return threatDouble;
        }
        #endregion

        //calculate dpr button click event
        private void calculateButton_Click(object sender, EventArgs e)
        {
            //converting attack modifier, crit chance, and crit multiplier to doubles
            double attackModifier = Convert.ToDouble(attackModifierNumber.Value);
            double critChance = getThreatRange(dThreat0.Text);
            double critMulti = getCritMultiplier(dCrit0.Text);

            //getting the average damage values of each group of dice
            #region
            double diceType0 = getDiceType(dType0.Text);
            double diceType1 = getDiceType(dType1.Text);
            double diceType2 = getDiceType(dType2.Text);
            double diceType3 = getDiceType(dType3.Text);
            double diceType4 = getDiceType(dType4.Text);

            double critType0 = getDiceType(dCritType0.Text);
            double critType1 = getDiceType(dCritType1.Text);
            double critType2 = getDiceType(dCritType2.Text);
            double critType3 = getDiceType(dCritType3.Text);

            double diceNumber0 = Convert.ToDouble(dNumber0.Value);
            double diceNumber1 = Convert.ToDouble(dNumber1.Value);
            double diceNumber2 = Convert.ToDouble(dNumber2.Value);
            double diceNumber3 = Convert.ToDouble(dNumber3.Value);
            double diceNumber4 = Convert.ToDouble(dNumber4.Value);

            double critNumber0 = Convert.ToDouble(dCritNumber0.Value);
            double critNumber1 = Convert.ToDouble(dCritNumber1.Value);
            double critNumber2 = Convert.ToDouble(dCritNumber2.Value);
            double critNumber3 = Convert.ToDouble(dCritNumber3.Value);

            double damageModifier = Convert.ToDouble(dModifier0.Value);

            double averageDamage0 = (((diceType0+1)/2)*diceNumber0)+damageModifier;

            double averageDamage1 = 0;
            double averageDamage2 = 0;
            double averageDamage3 = 0;
            double averageDamage4 = 0;

            double critDamage0 = 0;
            double critDamage1 = 0;
            double critDamage2 = 0;
            double critDamage3 = 0;

            if (diceNumber1 > 0)
            {
                averageDamage1 = (((diceType1 + 1) / 2) * diceNumber1);
            }
            if (diceNumber2 > 0)
            {
                averageDamage2 = (((diceType2 + 1) / 2) * diceNumber2);
            }
            if (diceNumber3 > 0)
            {
                averageDamage3 = (((diceType3 + 1) / 2) * diceNumber3);
            }
            if (diceNumber4 > 0)
            {
                averageDamage4 = (((diceType4 + 1) / 2) * diceNumber4);
            }

            if (critNumber0 > 0)
            {
                critDamage0 = (((critType0 + 1) / 2) * critNumber0);
            }
            if (critNumber1 > 0)
            {
                critDamage1 = (((critType1 + 1) / 2) * critNumber1);
            }
            if (critNumber2 > 0)
            {
                critDamage2 = (((critType2 + 1) / 2) * critNumber2);
            }
            if (critNumber3 > 0)
            {
                critDamage3 = (((critType3 + 1) / 2) * critNumber3);
            }
            #endregion

            //adding together all average damage values
            double totalAverageDamage = averageDamage0 + averageDamage1 + averageDamage2 + averageDamage3 + averageDamage4;
            double totalAverageCritDamage = critDamage0 + critDamage1 + critDamage2 + critDamage3;

            //setting up string arrays for results text boxes
            string[] acTextBoxLines = new string[15];
            string[] hitChanceTextBoxLines = new string[15];
            string[] dprTextBoxLines = new string[15];

            //looping through each ac value and adjusting dpr accordingly, converting all double values back to strings
            int acStartValue = Convert.ToInt32(acStartNumber.Value)-7;
            if (acStartValue < 1)
            {
                acStartValue = 1;
            }
            int counter = 0;
            for (int i=acStartValue;i<acStartValue+15;i++)
            {
                double hitChance = 0.05;
                if (20 + attackModifier >= i)
                {
                    hitChance = ((21 + attackModifier) - (i + 1)) / 20;
                    if (hitChance < 0.05) // clamping hit chance between 5% and 95%, because a roll of 1 always misses and a roll of 20 always hits
                    {
                        hitChance = 0.05;
                    }
                    if (hitChance > 0.95)
                    {
                        hitChance = 0.95;
                    }
                }
                double adjustedDPR = (totalAverageDamage * hitChance) + ((totalAverageDamage+totalAverageCritDamage) * hitChance * critChance * critMulti);

                string iString = Convert.ToString(i) + ":";
                if (i == Convert.ToInt32(acStartNumber.Value))
                {
                    iString = "Enemy:";
                }
                string hitChanceString = Convert.ToString(hitChance*100);
                string dprString = Convert.ToString(adjustedDPR);
                acTextBoxLines[counter] = iString;
                hitChanceTextBoxLines[counter] = hitChanceString + "%";
                dprTextBoxLines[counter] = dprString;



                counter++;
            }

            //updating textboxes with new results
            acTextBox.Lines = acTextBoxLines;
            hitPercentTextBox.Lines = hitChanceTextBoxLines;
            dprTextBox.Lines = dprTextBoxLines;
        }
    }
}
